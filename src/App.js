import React from "react";
import Final from "./component/Usestate";
import Basics from "./component/Usestatebasics";
import Arrayusestate from "./component/Usestatearray";
import Objecrusestate from "./component/Usestateobject";

function App() {
  return (
    <div className="container">
      <h1>React Advanced</h1>
      <Final />
      <Basics />
      <Arrayusestate />
      <Objecrusestate />
    </div>
  );
}

export default App;
