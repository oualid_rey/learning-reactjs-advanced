import React, { useState } from "react";

const Usestatebasics = () => {
  const [text, setText] = useState("titre origine");
  function declencher() {
    if (text === "titre origine") {
      setText("titre changer");
    } else {
      setText("titre origine");
    }
  }
  return (
    <React.Fragment>
      <h1>Usestate exemple</h1>
      <h5>{text}</h5>
      <button type="button" className="btn" onClick={declencher}>
        changer le titre
      </button>

      <hr />
    </React.Fragment>
  );
};
export default Usestatebasics;
