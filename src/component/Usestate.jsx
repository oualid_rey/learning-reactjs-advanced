import React from "react";

const Usestate = () => {
  let titre = "declencher un event";

  function declancher() {
    alert("Hello Learning");
  }

  return (
    <React.Fragment>
      <h2>{titre}</h2>
      <button type="button" className="btn" onClick={declancher}>
        evenement
      </button>
      <hr />
    </React.Fragment>
  );
};
export default Usestate;
