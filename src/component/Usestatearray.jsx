import React from "react";
import { data } from "../data";

const Usestatearray = () => {
  const [people, setPeople] = React.useState(data);
  const deleteitem = (id) => {
    let newPeople = people.filter((person) => person.id !== id);
    setPeople(newPeople);
  };
  const edititem = () => {};
  return (
    <React.Fragment>
      <h2>Usestate array exemple</h2>
      {people.map((person) => {
        const { id, name } = person;
        return (
          <div key={id} className="item">
            <h4>
              {id}-{name}
            </h4>
            <button onClick={() => deleteitem(id)}>delete</button>
            <button onClick={() => edititem()}>edit</button>
          </div>
        );
      })}
      <button type="button" className="btn" onClick={() => setPeople([])}>
        delet all
      </button>
      <hr></hr>
    </React.Fragment>
  );
};

export default Usestatearray;
